function correctTitle(nameString) {
    
    const except = ["and", "the", "of", "in"]
    let words = nameString.split(",")

    const lowercaseTitle = []
    words.forEach(sentence => {
        lowercaseTitle.push(sentence.charAt(0).toUpperCase() + sentence.slice(1).toLowerCase())
    })

    const temp = lowercaseTitle.join(", ")
    const uppercaseTitle = temp.split(" ")

    let caseCorrectedTitle = []
    uppercaseTitle.forEach(sentence => {
        let words = sentence.split(" ")
        words.forEach(word => {
            if(!except.includes(word)) {
                caseCorrectedTitle.push(word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
            }
            else {
                caseCorrectedTitle.push(word)
            }
        })
    })

    caseCorrectedTitle = fixTitle(caseCorrectedTitle)
    
    return caseCorrectedTitle
}

function fixTitle(title) {
    title = title.join(" ")
    if(!title.includes(".")) {
        title = title + "."
    }
    title = title.replace("  ", " ")
    return title
}

console.log(correctTitle("jOn SnoW, kINg IN thE noRth"))
console.log(correctTitle("sansa stark,lady of winterfell."))
console.log(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN."));