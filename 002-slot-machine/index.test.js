"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
it("should return true", () => {
    const expected = true;
    const elements = ["@", "@", "@", "@"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
it("should return true", () => {
    const expected = true;
    const elements = ["abc", "abc", "abc", "abc"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
it("should return true", () => {
    const expected = true;
    const elements = ["SS", "SS", "SS", "SS"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
it("should return false", () => {
    const expected = false;
    const elements = ["&&", "&", "&&&", "&&&&"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
it("should return false", () => {
    const expected = false;
    const elements = ["SS", "SS", "SS", "Ss"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
it("should return false", () => {
    const expected = false;
    const elements = ["3, 4, 5", 345, [3, 4, 5], "345"];
    const actual = (0, _1.testJackpot)(elements);
    expect(actual).toBe(expected);
});
/*
it("should return false", () => {
    const expected = true;
    let first = {
        name: "Marie",
        home: "Trondheim"
    }

    let second = {
        name: "Marie",
        home: "Trondheim"
    }

    let third = {
        name: "Marie",
        home: "Trondheim"
    }

    let fourth = {
        name: "Marie",
        home: "Trondheim"
    }

    const elements = [first, second, third, fourth]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
}) */
//# sourceMappingURL=index.test.js.map