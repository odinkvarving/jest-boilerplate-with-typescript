import { testJackpot } from ".";

it("should return true", () => {
    const expected = true;
    const elements = ["@", "@", "@", "@"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

it("should return true", () => {
    const expected = true;
    const elements = ["abc", "abc", "abc", "abc"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

it("should return true", () => {
    const expected = true;
    const elements = ["SS", "SS", "SS", "SS"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

it("should return false", () => {
    const expected = false;
    const elements = ["&&", "&", "&&&", "&&&&"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

it("should return false", () => {
    const expected = false;
    const elements = ["SS", "SS", "SS", "Ss"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

it("should return false", () => {
    const expected = false;
    const elements = ["3, 4, 5", 345, [3, 4, 5], "345"]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
})

/*
it("should return false", () => {
    const expected = true;
    let first = {
        name: "Marie",
        home: "Trondheim"
    }

    let second = {
        name: "Marie",
        home: "Trondheim"
    }

    let third = {
        name: "Marie", 
        home: "Trondheim"
    }

    let fourth = {
        name: "Marie",
        home: "Trondheim"
    }

    const elements = [first, second, third, fourth]

    const actual = testJackpot(elements)

    expect(actual).toBe(expected)
}) */

