"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.points = void 0;
function points(twoPointers, threePointers) {
    // Validation
    if (twoPointers < 0) {
        throw new Error("Negative two-pointers are not allowed!");
    }
    if (threePointers < 0) {
        throw new Error("Negative three-pointers are not allowed!");
    }
    return twoPointers * 2 + threePointers * 3;
}
exports.points = points;
//# sourceMappingURL=index.js.map