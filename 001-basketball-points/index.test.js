"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
// (points(1, 1) ==> 5)
it("should add 1 two-pointer and 1 three-pointer to equal 5", () => {
    // Arrange
    const expected = 5;
    // Act
    const actual = (0, _1.points)(1, 1);
    // Assert
    expect(actual).toBe(expected);
});
it("should throw an error for negative two-pointers", () => {
    // Arrange, Act & Assert
    expect(() => (0, _1.points)(-1, 1)).toThrow("Negative two-pointers are not allowed!");
});
it("should throw an error for negative three-pointers", () => {
    // Arrange, Act & Assert
    expect(() => (0, _1.points)(1, -1)).toThrow("Negative three-pointers are not allowed!");
});
//# sourceMappingURL=index.test.js.map