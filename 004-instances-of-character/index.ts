
export function charCount(char, sentence) {
    
    if(char.length > 1) {
        console.log("error")
        return -1;
    }
    
    let charIndexes: number[] = []

    let index: number = sentence.indexOf(char)
    while(index != -1) {
        charIndexes.push(index)
        index = sentence.indexOf(char, index + 1)
    }

    console.log(charIndexes.length)
    return charIndexes.length
}

charCount('wow', "the universe is wow")
