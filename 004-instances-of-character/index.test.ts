import { charCount } from "." 

it("should return 1", () => {
    const expected = 1;

    const actual = charCount('a', "edabit")

    expect(actual).toBe(expected)
})

it("upper case should be excluded and return 1", () => {
    const expected = 1;

    const actual = charCount('c', "Chamber of secrets")

    expect(actual).toBe(expected)
})

it("upper case should be excluded and return 0", () => {
    const expected = 0;

    const actual = charCount('B', "boxes are fun")

    expect(actual).toBe(expected)
})

it("multiple occurences should return 4", () => {
    const expected = 4;

    const actual = charCount('b', "big fat bubbles")

    expect(actual).toBe(expected)
})

it("char no present should return 0", () => {
    const expected = 0;

    const actual = charCount('e', "javascript is good")

    expect(actual).toBe(expected)
})

it("special characters should be counted and return 2", () => {
    const expected = 2;

    const actual = charCount('!', "!easy!")

    expect(actual).toBe(expected)
})

it("multiple chars should return error", () => {
    const expected = -1;

    const actual = charCount('wow', "the universe is wow")

    expect(actual).toBe(expected)
})