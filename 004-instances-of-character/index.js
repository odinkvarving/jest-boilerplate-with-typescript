"use strict";
exports.__esModule = true;
exports.charCount = void 0;
function charCount(char, sentence) {
    if (char.length > 1) {
        console.log("error");
        return -1;
    }
    var charIndexes = [];
    var index = sentence.indexOf(char);
    while (index != -1) {
        charIndexes.push(index);
        index = sentence.indexOf(char, index + 1);
    }
    console.log(charIndexes.length);
    return charIndexes.length;
}
exports.charCount = charCount;
charCount('wow', "the universe is wow");
