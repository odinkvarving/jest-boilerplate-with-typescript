"use strict";
exports.__esModule = true;
var _1 = require(".");
it("should return 1", function () {
    var expected = 1;
    var actual = (0, _1.charCount)('a', "edabit");
    expect(actual).toBe(expected);
});
it("upper case should be excluded and return 1", function () {
    var expected = 1;
    var actual = (0, _1.charCount)('c', "Chamber of secrets");
    expect(actual).toBe(expected);
});
it("upper case should be excluded and return 0", function () {
    var expected = 0;
    var actual = (0, _1.charCount)('B', "boxes are fun");
    expect(actual).toBe(expected);
});
it("multiple occurences should return 4", function () {
    var expected = 4;
    var actual = (0, _1.charCount)('b', "big fat bubbles");
    expect(actual).toBe(expected);
});
it("char no present should return 0", function () {
    var expected = 0;
    var actual = (0, _1.charCount)('e', "javascript is good");
    expect(actual).toBe(expected);
});
it("special characters should be counted and return 2", function () {
    var expected = 2;
    var actual = (0, _1.charCount)('!', "!easy!");
    expect(actual).toBe(expected);
});
it("multiple chars should return error", function () {
    var expected = -1;
    var actual = (0, _1.charCount)('wow', "the universe is wow");
    expect(actual).toBe(expected);
});
