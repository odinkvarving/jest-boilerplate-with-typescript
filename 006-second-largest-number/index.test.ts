import secondLargest from "./index"

it("should return 40", () => {
    const expected = 40

    const numberArray: number[] = [10, 40, 30, 20, 50]

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})

it("should return 40", () => {
    const expected = 105

    const numberArray: number[] = [25, 143, 89, 13, 105]

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})

it("should return 40", () => {
    const expected = 23

    const numberArray: number[] = [54, 23, 11, 17, 10]

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})

it("should return 40", () => {
    const expected = 0

    const numberArray: number[] = [1, 1]

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})

it("should return 40", () => {
    const expected = 1

    const numberArray: number[] = [1]

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})

it("should return 40", () => {
    const expected = 0

    const numberArray: number[] = []

    const actual = secondLargest(numberArray)

    expect(actual).toBe(expected)
})