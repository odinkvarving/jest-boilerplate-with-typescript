function secondLargest(numberArray: number[]) {

    if(numberArray.length == 0 || (numberArray.length === 2 && numberArray[0] === numberArray[1])) {
        return 0
    }
    else if(numberArray.length === 1) {
        return numberArray[0]
    }
    else if((numberArray.length === 2 && numberArray[0] !== numberArray[1]) || numberArray.length > 2) {
        return numberArray.sort((a, b) => b - a)[1]
    }
}

export default secondLargest





