function elasticize(sentence) {
    let charArray = [...sentence]

    if(sentence.length % 2 == 1) {
        const half = Math.ceil(charArray.length / 2)
        const firstHalf = charArray.splice(0, half - 1)
        const middle = charArray.splice(0, 1)
        const secondHalf = charArray.splice(-half)

        let checkedChars = []
        let newString = ""
        for(let i = 0; i < firstHalf.length; i++) {
            let char = firstHalf[i]
            checkedChars.push(char)
            if(checkedChars.includes(char)) {
                newString += char.repeat(checkedChars.indexOf(char))
            }
            else {
                newString += char.repeat(i)
            }
        }

        console.log(newString)
        
    }
    else {
        const half = Math.ceil(charArray.length / 2 - 1)
        const firstHalf = charArray.splice(0, half + 1)
        const secondHalf = charArray.splice(-half - 1)

        console.log(firstHalf)
        console.log(secondHalf)
    }
}

elasticize("AANNKAABB")