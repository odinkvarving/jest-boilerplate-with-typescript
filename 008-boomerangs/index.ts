export function countBoomerangs(numberArray): number | string {
    
    if(!Array.isArray(numberArray) || numberArray.length < 3 || numberArray.some(isNaN)) {
        return "error" 
    }

    let counter: number = 0
    for(var i = 0; i < numberArray.length - 2; i++) {
            if((numberArray[i] !== numberArray[i + 1]) && numberArray[i] == numberArray[i + 2]) {
                counter++
            }
        }
    return counter
}