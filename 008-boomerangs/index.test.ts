import { countBoomerangs } from "."

it("should return 2", () => {
    const expected = 2

    const numberArray: number[] = [9, 5, 9, 5, 1, 1, 1]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return 1", () => {
    const expected = 1

    const numberArray: number[] = [5, 6, 6, 7, 6, 3, 9]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return 0", () => {
    const expected = 0

    const numberArray: number[] = [4, 4, 4, 9, 9, 9, 9]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return 5", () => {
    const expected = 5

    const numberArray: number[] = [1, 7, 1, 7, 1, 7, 1]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return error", () => {
    const expected = "error"

    const numberArray: number[] = []

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return error", () => {
    const expected = "error"

    const numberArray: number[] = [1, 7]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})

it("should return error", () => {
    const expected = "error"

    const numberArray: any = [1, 7, 1, 7, 'one', 7, 1]

    const actual = countBoomerangs(numberArray)

    expect(actual).toBe(expected)
})



